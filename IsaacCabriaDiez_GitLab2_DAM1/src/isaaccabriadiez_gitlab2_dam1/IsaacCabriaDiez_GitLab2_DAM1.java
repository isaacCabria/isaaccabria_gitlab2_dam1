/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isaaccabriadiez_gitlab2_dam1;

import java.util.Scanner;

/**
 *
 * @author DAM103
 */
public class IsaacCabriaDiez_GitLab2_DAM1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);
        int hora;
        double pie;
        double pulgada;
        double milla;
        int km;
        double kms;
        int menu;
        int menu2;
        System.out.println("Buenos dias estudiante");
        do {
            System.out.println("--ELIGE UNA OPCION--");
            System.out.println("1- Horas a segundos");
            System.out.println("2- kilometros a metros");
            System.out.println("3- km/s a m/s");
            System.out.println("4- Unidades imperiales");
            System.out.println("5- Salir");
            menu = teclado.nextInt();
            switch (menu) {
                case 1:
                    System.out.println("Introduce las horas");
                    hora = teclado.nextInt();
                    System.out.println("Las horas a segundos son: " + hora * 3600);
                    break;
                case 2:
                    System.out.println("Introduce los kilometros");
                    km = teclado.nextInt();
                    System.out.println("Los kilometros a metros son: " + km * 1000);
                    break;
                case 3:
                    System.out.println("Introduce los km/s");
                    kms = teclado.nextDouble();
                    System.out.printf("Los KM/S a M/S son: %,.2f",kms/3,6);
                break;
                case 4:
                    System.out.println("--Elige una opcion de unidades--");
                    System.out.println("1- Pies a Centímetros");
                    System.out.println("2- Pulgadas a Milímetros");
                    System.out.println("3- Millas a Kilómetros.");
                    System.out.println("4- Salir al menu principal");
                    menu2 = teclado.nextInt();
                    switch(menu2){
                        case 1:
                            System.out.println("Introduce los pies");
                            pie = teclado.nextDouble();
                            System.out.printf("Los pies a centimetros son: %,.2f",pie*30.48);
                        break;
                        case 2:
                            System.out.println("Introduce las pulgadas");
                            pulgada = teclado.nextDouble();
                            System.out.printf("Las pulgadas a milimetros son: %,.2f",pulgada*25.4);
                        break;
                        case 3:
                            System.out.println("Introduce las millas");
                            milla = teclado.nextDouble();
                            System.out.printf("Las millas a kilometros son: %,.2f",milla*1.60);
                        break;
                    }
            }
        } while (menu != 5);
        System.out.println("¡Hasta pronto!");

    }
    
}
